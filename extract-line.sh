#!/bin/bash
calc() { awk "BEGIN{print $*}"; }
calc $(cat $1 | grep -c X)/$(cat $1 | wc -c)*100;
