/// Returns the prime numbers in `elements`.
func primes(_ numbers: [Int]) -> [Int] {
  var primes: [Int] = []

  for n in numbers {
    var isPrime = true
    for m in 2 ..< n {
      if n % m == 0 {
        isPrime = false
        break
      }
    }

    if isPrime {
      primes.append(n)
    }
  }

  return primes
}

let array = Array(2 ..< 50)
let primesInArray = primes(array)
print("\(array) contains the following prime numbers: \(primesInArray)")
